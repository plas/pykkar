# coding=UTF-8

from pykkar import *

# Ülesanne: ristkülikukujulise vaba ala laius on n, kõrgus vähemalt 3
# kirdenurgas on vähemalt n liiklustorbikut (cone)
# need vaja viia lõunaseina äärde (igale ruudule 1 torbik)


# Maailma kaardi sümbolite tähendust vaata moodulist pykkar
create_world("""
#######
# >  5#
#     #
#     #
#     #
#     #
#     #
#######
""")

# abifunktsioonid
def left():
    right()
    right()
    right()

def turn_around():
    right()
    right()
    
def go_to_obstacle():
    while not is_wall() and not is_cone():
        step() 

def go_take_cone():
    while get_heading() != E:
        right()
        
    go_to_obstacle()
    
    if not is_cone():
        left()    
        go_to_obstacle()
        
    assert is_cone()
    take()

def go_put_cone():
    while get_heading() != W:
        right()
    go_to_obstacle()
    left()
    go_to_obstacle()
    left()
    go_to_obstacle()
    left()
    step()
    turn_around()
    put()
    
def at_leftmost_cone():
    right()
    result = is_wall()
    left()
    return result

set_speed(10)

while True:
    go_take_cone()
    go_put_cone()
    if at_leftmost_cone():
        break

    