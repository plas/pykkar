from pykkar import *

create_world("""
########
#      #
#      #
#      #
#      #
#      #
#    > #
#      #
########
""")


# face north

if get_direction() == "E":
    right()

if get_direction() == "S":
    right()

if get_direction() == "W":
    right()
