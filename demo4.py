from pykkar import *

create_world("""
##########
#>.  .   #
##########
""")

painted_count = 0 

while True:
    if is_painted():
        painted_count += 1
    
    step()
    
    if is_wall():
        break

print(painted_count)