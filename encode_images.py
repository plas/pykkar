""" used for generating base64 representation of Pykkar gif-s"""
import os
import base64
import textwrap



IMG_FOLDER = "."

print("_image_data = {")

for name in os.listdir(IMG_FOLDER):
    if name.endswith(".gif"):
        f = open(IMG_FOLDER + "/" + name, mode='rb')
        data = f.read()
        f.close()
        data_str = "\n        ".join(textwrap.wrap(str(base64.b64encode(data), encoding="ASCII")))
         
        print("    '%s' : \"\"\"\n        %s\"\"\"," % (name[:-4], data_str))
print("}")