from pykkar import *
import tkinter as tk

create_world("""
########
#      #
#      #
#      #
#      #
#      #
#    > #
#      #
########
""")

def on_key(event):
    print(get_x())
    if event.keysym == "Up":
        step()
    else:
        right()

bind("<Key>", on_key)
mainloop()