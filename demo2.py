# coding=UTF-8

from pykkar import *

# Ülesanne: värvida ristkülikukujuline ala maleruudu stiilis
# Maailma kaardi sümbolite tähendust vaata moodulist pykkar

create_world("""
##########
# >      #
#        #
#        #
#        #
#        #
#        #
#        #
#        #
##########
""")



def left():
    old_speed = get_speed()
    set_speed(10)
    right()
    right()
    right()
    set_speed(old_speed)

def go_to_wall():
    while not is_wall():
        step()

def turn_around():
    right()
    right()

def paint_checkerboard():
    set_speed(10)
    
    while get_heading() != N:
        right()
    
    go_to_wall()
    left()
    go_to_wall()
    turn_around()
    step_no = 0
    direction = 'right'
    while True:
        if step_no % 2 == 0:
            paint()
        
        if is_wall():
            if direction == 'right':
                right()
                if is_wall():
                    break
                step()
                right()
                direction = 'left'
            else:
                left()
                if is_wall():
                    break
                step()
                left()
                direction = 'right'
            
        else:
            step()
        step_no += 1
        
        
paint_checkerboard()

