# coding=UTF-8

from pykkar import *

# Ülesanne: ristkülikukujulise vaba ala laius on n, kõrgus vähemalt 3
# kirdenurgas on vähemalt n liiklustorbikut (cone)
# need vaja viia lõunaseina äärde (igale ruudule 1 torbik)


# Maailma kaardi sümbolite tähendust vaata moodulist pykkar
create_world("""
#######
# >  5#
#     #
#     #
#     #
#     #
#     #
#######
""")

while get_direction() != "W":
    right()
