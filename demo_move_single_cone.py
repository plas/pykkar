from pykkar import *

create_world("""
###############
#1   >        #
###############
""")

right()
right()

while not is_cone():
    step()

take()

right()
right()

while not is_wall():
    step()

right()
right()
step()
right()
right()
put()

