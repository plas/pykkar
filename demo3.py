# coding=UTF-8

from pykkar import World, Pykkar, N

# Ülesanne: käi mööda seina- (ja seina ääres olevate asjade) ääri
# Maailma kaardi sümbolite tähendust vaata moodulist pykkar

# Näide OOP stiili kohta



class MyPykkar(Pykkar):
    def left(self):
        old_speed = self.get_speed()
        self.set_speed(10)
        self.right()
        self.right()
        self.right()
        self.set_speed(old_speed)
    
    def go_to_obstacle(self):
        while not self.is_obstacle():
            self.step()
    
    def is_obstacle(self):
        return self.is_wall() or self.is_box() or self.is_cone()
    
    def turn_around(self):
        self.right()
        self.right()
        
    def walk_by_the_walls(self):
        self.go_to_obstacle()
        
        while True:
            self.left()
            if self.is_obstacle():
                self.right()
            if self.is_obstacle():
                self.right()
            if self.is_obstacle():
                self.right()
            if self.is_obstacle():
                self.right
            else:
                self.step()
            
        
p = MyPykkar(World("""
#########
# >  b  #
#       #
#  3    #
#2      #
#   #   #
#   #   #
#########
""")) 

p.set_speed(10)
p.walk_by_the_walls()

